import { assert } from "chai";

const EMPTY_SPACE = '.';

export class Board {
  width;
  height;
  middle;
  boardLines;
  hasBlockFalling;

  constructor(width, height) {
    this.width = width;
    this.height = height;
    this.middle = Math.floor(this.width / 2);
    this.boardLines = Array(height).fill().map(() => Array(width).fill(EMPTY_SPACE));
  }

  toString() {
    return this.boardLines.map((x) => x.join('')).join('\n') + '\n';
  }

  drop(block) {
    assert(!this.hasBlockFalling, 'already falling');

    this.hasBlockFalling = true;
    this.boardLines[0][this.middle] = block;
  }

  tick() {
    let hasBlockThatFell = false;
    for (let x = 0; x < this.width; x++) {
      for (let y = this.height - 2; y >= 0; y--) {
        const element = this.boardLines[y][x];
        const elementBelow = this.boardLines[y + 1][x];

        if (EMPTY_SPACE != element && EMPTY_SPACE == elementBelow) {
          hasBlockThatFell = true;
          this.boardLines[y + 1][x] = element;
          this.boardLines[y][x] = EMPTY_SPACE;
        }
      }
    }

    this.hasBlockFalling = hasBlockThatFell;
  }

  hasFalling() {
    return this.hasBlockFalling;
  }
}
