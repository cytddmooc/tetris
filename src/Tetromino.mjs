export class Tetromino {

    static T_SHAPE = `.T.
TTT
...
`;

    static I_SHAPE = `.....
.....
IIII.
.....
.....
`;

    static O_SHAPE = `.OO
.OO
...
`;

    configuration;

    constructor(configuration) {
        this.configuration = configuration;
    }

    toString() {
        return this.configuration + '\n';
    }
}
