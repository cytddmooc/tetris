export class RotatingShape {
    shapeGrid;

    constructor(shape) {
        this.shapeGrid = shape.split('\n').map((x) => x.trim().split(''));
    }

    toString() {
        return this.getString(this.shapeGrid) + '\n';
    }

    getString(grid) {
        return grid.map((line) => line.join('')).join('\n');
    }

    rotateRight() {
        let grid = Array(this.shapeGrid.length).fill().map((_) => Array(this.shapeGrid[0].length));

        const maxWidth = this.shapeGrid[0].length - 1;
        for (let h = 0; h < this.shapeGrid.length; h++) {
            for (let w = 0; w < this.shapeGrid[0].length; w++) {
                grid[h][w] = this.shapeGrid[maxWidth - w][h];
            }
        }

        return new RotatingShape(this.getString(grid));
    }

    rotateLeft() {
        let grid = Array(this.shapeGrid.length).fill().map((_) => Array(this.shapeGrid[0].length));

        const maxHeight = this.shapeGrid.length - 1;
        for (let h = 0; h < this.shapeGrid.length; h++) {
            for (let w = 0; w < this.shapeGrid[0].length; w++) {
                grid[h][w] = this.shapeGrid[w][maxHeight - h];
            }
        }

        return new RotatingShape(this.getString(grid));
    }
}
